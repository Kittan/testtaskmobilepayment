import Router, { useRouter } from 'next/router';
import MainLayout from '../MainLayout';
import { Container, Button, Input } from '../../component/styled';
import { useState } from 'react';
import InputMask from 'react-input-mask';
import { ToastContainer, toast } from 'react-toastify';

type TState = {
  phone: string,
  price: number,
}

const Operator = () => {
  const [state, setState] = useState<TState>({ phone: "+7", price: 0 })
  const [buttonDisabled, setButtonDisabled] = useState<boolean>(false);

  const operator = useRouter().query.name;

  const onClickButton = (e) => {
    e.preventDefault();
    if (buttonDisabled) return;

    if (!state.price || state.phone.length < 16) {
      return toast.error('Введите коректные данные');
    }

    setButtonDisabled(true)
    setTimeout(() => {
      if (Math.random() > 0.50) {
        toast.success('Пополнение прошло успешно');
        setTimeout(() => {
          Router.push('/')
        }, 3000)
      } else {
        toast.error("Попытайтесь ещё!");
        setButtonDisabled(false)
      }
    }, 1000)
  }


  const onChangeInputValue = (e: React.ChangeEvent<HTMLInputElement>, name: string) => {
    e.persist();
    if (name === 'price') {
      if (+e.target.value > 0 && +e.target.value <= 1000 || e.target.value === '') {
        setState(prev => ({ ...prev, [name]: +e.target.value }))
      }
    } else {
      setState(prev => ({ ...prev, [name]: e.target.value }))
    }
  }

  return (
    <MainLayout title="Пополнение счёта">
      <Container>
        <form className='container-form form-payment' onSubmit={(e) => onClickButton(e)}>
          <h3 className='operator-title'>{operator}</h3>
          <InputMask
            mask="+7 999 999 99 99"
            disabled={false}
            maskChar=''
            value={state.phone}
            onChange={e => onChangeInputValue(e, "phone")} >
            {() => <Input type="tel" placeholder={"Номер телефона"} />}
          </InputMask>
          <Input
            value={state.price || ''}
            type="number"
            placeholder='Сумма платежа (1-1000 руб)'
            onChange={e => onChangeInputValue(e, "price")} />
          <Button page='payment'>Оплатить</Button>
        </form>
      </Container>
      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </MainLayout>
  )
}

export default Operator