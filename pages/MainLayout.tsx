import Head from 'next/head';

type TProps = {
  title: string,
  children: React.ReactNode
}

const MainLayout = ({title, children }: TProps) => {
  return (
    <>
      <Head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{title} | Оплата мобильного телефона </title>
      </Head>
      <header>
        <h1>Оплата мобильного телефона</h1>
      </header>
      <main>
      {children}
      </main>
      <footer></footer>
    </>
  )
}
export default MainLayout;
