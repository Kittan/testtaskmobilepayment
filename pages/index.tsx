import MainLayout from "./MainLayout";
import { Input, Button, ListItem, Container } from '../component/styled';
import { useState } from "react";
import Router from 'next/router';
import { ToastContainer, toast } from 'react-toastify';

const Index = () => {
  const [stateList, setStateList] = useState<string[]>(['мтс', 'билайн', 'мегафон'])
  const [stateInput, setStateInput] = useState<string>('');
  const [animation, setAnimation] = useState<boolean>(false);

  const addOperator = (e) => {
    e.preventDefault();

    if(stateList.includes(stateInput.toLowerCase())){
      return toast.error('Оператор уже есть в списке!')
    }
    setStateList((prev) => ([...prev, stateInput.toLowerCase()]))
    setStateInput('')
  }

  const goToOperator = (name: string) => {
    setAnimation(true)
    setTimeout(() => {
      Router.push(`/operator/${name}`)
    }, 1000)
  }

  return (
    <MainLayout title="Выбор оператора">
      <Container animation={animation}>
        <h2 className='container-title'>Выберите мобильного оператора</h2>
        <form className='container-form' onSubmit={(e) => addOperator(e)}>
          <Input
            value={stateInput}
            type="text"
            placeholder='Название'
            onChange={({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => setStateInput(value)}
            required />
          <Button>Добавить Оператора</Button>
        </form>
        <div className='list'>
          {stateList.map((list, i) => (
            <ListItem key={i} onClick={() => goToOperator(list)}>
              <span className="circle">
                <span className="icon arrow"></span>
              </span>
              <span className="button-text">{list}</span>
            </ListItem>
          ))}
        </div>
      </Container>
      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </MainLayout>
  )
}

export default Index;