import '../styles/globals.css';
import '../styles/style.css';
import 'react-toastify/dist/ReactToastify.css';
export default function MyApp ({Component, pageProps}) {
  return (
    <Component {...pageProps}/>
  )
}
