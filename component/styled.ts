import styled from 'styled-components';


const Input = styled.input`
  font-family: inherit;
  width: 100%;
  border: 0;
  border-bottom: 2px solid #9b9b9b;
  outline: 0;
  font-size: 1.3rem;
  color: white;
  padding: 7px 0;
  background: transparent;
  transition: border-color 0.2s;
  &:focus {
    border-width: 3px;
    border-color: white;
  },
  &::placeholder {
    color: white;
  }
`

const Button = styled.button<{page?:string}>`
  padding: 10px;
  border: 2px solid black;
  border-radius: 10px;
  transition: all 300ms ease;
  box-shadow: 0px 4px 10px 2px rgba(black, 0.2);
  position: relative;
  display: inline-block;
  color: white;
  background-color: transparent;
  cursor: pointer;
  font-size: 15px;
  outline: none;
  margin-left: 10px;
  ${props => props.page == 'payment' ? 'margin: 0;': ''}
  &::before {
    position: absolute;
    content: "";
    width: 0%;
    height: 100%;
    background: white;
    top: 0;
    left: auto;
    right: 0;
    z-index: -1;
    transition: all 300ms ease;
    border-radius: 5px;
  }
  &:hover {
    &{
      color: black;
      box-shadow: none;
    }
    
    &::before {
      position: absolute;
      content: "";
      width: 100%;
      height: 100%;
      background: white;
      top: 0;
      left: 0;
      right: 0;
      z-index: -1;
      border-radius: 5px;
    }
  }
`


const Container = styled.div<{animation?: boolean}>`
  height:  80%;
  width: 40%;
  border: 1px solid black;
  border-radius: 20px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  background: linear-gradient(135deg, rgba(75,8,109,1) 0%, rgba(172,192,254,1) 100%);
  color: white;
  padding: 10px;
  transition: 1s ease;
  ${props => (props.animation 
    ? "transform: translateX(1536px);" 
    : '')}
  @media screen and (max-width: 1150px) {
    width:50%;
  }
  @media screen and (max-width: 1000px) {
    width:60%;
  }
  @media screen and (max-width: 800px) {
    width:70%;
  }
  @media screen and (max-width: 650px) {
    width:80%;
  }
  @media screen and (max-width: 550px) {
    width:100%;
  }
`

const ListItem = styled.button`
  position: relative;
  display: inline-block;
  cursor: pointer;
  outline: none;
  border: 0;
  vertical-align: middle;
  text-decoration: none;
  background: transparent;
  padding: 0;
  font-size: inherit;
  font-family: inherit;
  width: 98%;
  height: auto;
  margin: 5px 0;
  .circle {
    transition: all 0.45s cubic-bezier(0.65,0,.076,1);
    position: relative;
    display: block;
    margin: 0;
    width: 3rem;
    height: 3rem;
    background: black;
    border-radius: 1.625rem;
    .icon {
      transition: all 0.45s cubic-bezier(0.65,0,.076,1);
      position: absolute;
      top: 0;
      bottom: 0;
      margin: auto;
      background: white;
      &.arrow {
        transition: all 0.45s cubic-bezier(0.65,0,.076,1);
        left: 0.625rem;
        width: 1.125rem;
        height: 0.125rem;
        background: none;
        &::before {
          position: absolute;
          content: '';
          top: -0.30rem;
          right: 0.0625rem;
          width: 0.625rem;
          height: 0.625rem;
          border-top: 0.155rem solid #fff;
          border-right: 0.155rem solid #fff;
          transform: rotate(45deg);
        }
      }
    }
  }
  .button-text {
    transition: all 0.45s cubic-bezier(0.65,0,.076,1);
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    padding: 0.75rem 0.50rem 0.75rem 1.50rem;
    margin: 0 0 0 1.85rem;
    color: black;
    font-weight: 700;
    line-height: 1.6;
    text-align: center;
    text-transform: uppercase;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  &:hover {
    .circle {
      width: 100%;
      .icon {
        &.arrow {
        background: white;
        transform: translate(1rem, 0);
        }
      }
    }
    .button-text {
      color: white;
    }
  }
`

export { Input, Button, ListItem, Container }